function [bound] = find_distrib_percent(x,opt)
%
% compute hist of x data and extract part of it centered on opt
%
%%% INPUTS
% x   : data vector
% opt : 
%       opt.lim    = % percentiles extreme selected
%       opt.flag   = 1 or 0 (plot option)
%
%%% OUTPUTS
% bound : selected boundaries
%
%
%%% 
% Gaetan 2015/01/15


% set defaults
%---------------
if ~isfield(opt,'lim'), opt.lim = 30;else end
if ~isfield(opt,'flag'), opt.flag = 0;else end
%-----------------

limit  = opt.lim;

bound = [prctile(x,limit) prctile(x,100-limit)]; % Boundaries
        

if opt.flag
  % plot
  %------
  n = hist(x,round(numel(x)/5));
  figure;set(gcf,'color','w');
  hist(x,round(numel(x)/5)); hold on;
  
  Ylim = [0 max(n)];

  Xlim = [bound(1) bound(1)];
  line(Xlim,Ylim,'LineStyle','--','color','r','LineWidth',2);
  Xlim = [bound(2) bound(2)];
  line(Xlim,Ylim,'LineStyle','--','color','r','LineWidth',2);
  
  
  xlabel('Power','FontSize',15,'FontWeight','bold');
  set(gca,'FontSize',10,'FontWeight','bold','LineWidth',2);
  title('Data distribution','FontSize',15,'FontWeight','bold');
  
else
end