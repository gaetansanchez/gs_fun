function [Tab] = Do_extractROIname_atlas(A,sigVox,sigVal,data,savefilename,queryrange)
% extract label of ROI based on an atlas
%
%%% INPUTS
%-----------
% A      = atlas fieldtrip structure
% sigVox = significant voxels indices
% sigVal = values (T-value or Accuracy) for each voxel indices
% data   = fieldtrip data structure with pos field = matrix of ROI coordinates 
% ==> should be same units and coordinates system as atlas coordinates
% savefilename = path and name file to save output (no save if savefilename = [])
%
%%% OUTPUTS
%-----------
% Tab = matrix of labels and coordinates
%
if nargin < 6
  queryrange = 1; % default range for atlas label 
end


Tab = {' '};
n = 1;
nVox = numel(sigVox);
for i = 1:nVox
  
  pos = data.pos(sigVox(i),:);
  [labatlas{i}] = atlas_lookup_gs(A, pos,'inputcoord',A.coordsys,'queryrange',queryrange);
  
  % check if several ROI labels for the same location
  %--------------------------------------------------
  lab2note = [];
  if ~isempty(labatlas{i})
    if numel(labatlas{i})>1
      for j = 1:numel(labatlas{i})
        lab2note = [lab2note '  ' labatlas{i}{j}];
      end
    else
      lab2note = labatlas{i}{1};
    end
  end
  
  % fill-in the table only if there is a ROI & not already present in Tab
  %----------------------------------------------------------------------
  if ~isempty(lab2note) && ~ismember({lab2note},[Tab(:,1)])
   
    Tab{n,1} = lab2note;
    Tab{n,2} = pos(1);
    Tab{n,3} = pos(2);
    Tab{n,4} = pos(3);
    Tab{n,5} = sigVox(i); % voxel indice
    
    for k = 1:size(sigVal,2)
      Tab{n,5+k} = sigVal(i,k); % all values
    end
    
    n = n+1;
  else
  end
  
  
end

% Save if needed
%---------------
if ~isempty(savefilename)
  save(savefilename, 'Tab');
end

