function [h] = plot_3by3_MVPA(data,cfg)
% function plot_3by3_MVPA(data,cfg)
% plots 3by3 classifier accuracy with mask using imagesc
%
% Mandatory Input:
% data         - [m*n*o*p] conds*conds*time1*time2
%
%
% Optional [defaults]:
% cfg.xaxis    - vector of timepoints [1:size(data(1,1,:,:),2)]
% cfg.fontsize - numeric, fontsize [14]
% cfg.LimAx    - min and max of colormap (default = [0.5 0.53])
% cfg.smooth   - numeric, smoothing iterations (interpolation)
% cfg.mask     - [m*n*o*p] (same as data) used to mask, e.g.,
%                non-significant values, translates to the alpha level in
%                matlab (0 = completely transparent; 1 = no transparency)
% cfg.mask_int - 'nearest', 'linear', or 'spline' - way of interpolating
%                the mask field ['nearest']
%
%%%
%%
if nargin < 2, cfg = []; end
% defaults
if isfield(cfg, 'mask'), cfg.mask = double(cfg.mask); end
if ~isfield(cfg, 'LimAx'), cfg.LimAx = [0.5 0.53]; end
if isfield(cfg, 'fontsize'), fsize = cfg.fontsize; else fsize = 15; end
if ~isfield(cfg, 'xaxis'), cfg.xaxis = 1:size(data, 3); end

ntest  = numel(cfg.xaxis);

if ~isfield(cfg, 'xytick'),nticks = 5; cfg.xytick = round(linspace(1, ntest, nticks))'; end

h = figure;
set(h,'color','white');

%% smoothing
%-----------
for i = 1:size(data,1)
  for j = 1:size(data,2)
    
    if isfield(cfg, 'smooth')
      % do smoothing now with interpolation
      plotData(i,j,:,:) = interp2(squeeze(data(i,j,:,:)),cfg.smooth);
      if isfield(cfg, 'mask') % smooth also mask field if present
        if isfield(cfg, 'mask_int')
          maskData(i,j,:,:) = interp2(squeeze(cfg.mask(i,j,:,:)),cfg.smooth, cfg.mask_int);
        else
          maskData(i,j,:,:) = interp2(squeeze(cfg.mask(i,j,:,:)),cfg.smooth, 'nearest');
        end
      end
    else
      plotData(i,j,:,:) = squeeze(data(i,j,:,:));
      if isfield(cfg, 'mask')
        maskData(i,j,:,:) = squeeze(cfg.mask(i,j,:,:));
      end
    end
    
  end
end

%%

labels = {'Training_time_(ms)' 'Testing_time_(ms)'};

kplot = 1;
for i = 1:size(plotData,1)
  
  for j = 1:size(plotData,2)
    
    subplot(size(plotData,1),size(plotData,2),kplot);
    
    try
      plotDatasq = reshape(squeeze(plotData(i,j,:,:)),ntest,ntest);
    catch
      plotDatasq = squeeze(plotData(i,j,:,:));
    end
    
    % show the results
    %surf(1:size(data,1),1:size(data,2),data');
    
    imagesc(plotDatasq, [cfg.xaxis(1) cfg.xaxis(end)]);
    set(gca,'Ydir','normal');
    
    if isfield(cfg, 'mask')
      % set alpha according to mask field
      alpha(squeeze(maskData(i,j,:,:)));
    end
    
    
    fact_smooth = size(plotDatasq,1)/ntest;
    
    xon  = 0;
    %yoff = ytick_corr(end)+(fact_smooth/2);
    yoff = round(numel(cfg.xaxis)*fact_smooth)+(fact_smooth/2);
    
    ytick = cfg.xytick;
    ytick_corr = round(ytick*fact_smooth)-(fact_smooth/2);
    
    Tlab  = cfg.xaxis(ytick);
    YTlab = cellfun(@num2str,num2cell(Tlab),'UniformOutput', false);
    set(gca,'Ytick',ytick_corr,'YTickLabel',YTlab);
    
    xtick = cfg.xytick;
    xtick_corr = round(xtick*fact_smooth)-(fact_smooth/2);
    
    Tlab  = cfg.xaxis(ytick);
    XTlab = cellfun(@num2str,num2cell(Tlab),'UniformOutput', false);
    set(gca,'Xtick',xtick_corr,'XTickLabel',XTlab);
    
    if i == 1 && j == 1
      xlabel(strrep(labels{2},'_',' '),'FontSize',17,'FontWeight','bold');
      ylabel(strrep(labels{1},'_',' '),'FontSize',17,'FontWeight','bold');
      
      
    end
    if i ~= j
      set(gca,'ycolor','r','xcolor','r');
    end
    
    
    hold on;
    plot([yoff xon], [yoff xon], 'k:','Linewidth',2);  %change r: as needed
    
    hold off;
    colorbar();
    %caxis([0.5 0.53]);
    %caxis([-0.1 0.1]);
    caxis(cfg.LimAx);
    set(gca,'FontSize',13,'FontWeight','bold','LineWidth',3);
    
    
    kplot = kplot+1;
    
  end
end
