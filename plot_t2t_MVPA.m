function [h] = plot_t2t_MVPA(data,cfg)
% function plot_3by3_MVPA(data,cfg)
% plots 3by3 classifier accuracy with mask using imagesc
%
% Mandatory Input:
% data         - [o*p] time1*time2
%
%
% Optional [defaults]:
% cfg.xaxis    - vector of timepoints [1:size(data(:,:),2)]
% cfg.fontsize - numeric, fontsize [14]
% cfg.LimAx    - min and max of colormap (default = [0.5 0.53])
% cfg.smooth   - numeric, smoothing iterations (interpolation)
% cfg.mask     - [o*p] (same as data) used to mask, e.g.,
%                non-significant values, translates to the alpha level in
%                matlab (0 = completely transparent; 1 = no transparency)
% cfg.mask_int - 'nearest', 'linear', or 'spline' - way of interpolating
%                the mask field ['nearest']
%
%%%
%%
if nargin < 2, cfg = []; end
% defaults
if isfield(cfg, 'mask'), cfg.mask = double(cfg.mask); end
if ~isfield(cfg, 'LimAx'), cfg.LimAx = [0.5 0.53]; end
if isfield(cfg, 'fontsize'), fsize = cfg.fontsize; else fsize = 15; end
if ~isfield(cfg, 'xaxis'), cfg.xaxis = 1:size(data, 3); end

ntest  = numel(cfg.xaxis);

if ~isfield(cfg, 'xytick'),nticks = 5; cfg.xytick = round(linspace(1, ntest, nticks))'; end

h = figure;
set(h,'color','white');

%% smoothing
%-----------

if isfield(cfg, 'smooth')
  % do smoothing now with interpolation
  plotData(:,:) = interp2(squeeze(data(:,:)),cfg.smooth);
  if isfield(cfg, 'mask') % smooth also mask field if present
    if isfield(cfg, 'mask_int')
      maskData(:,:) = interp2(squeeze(cfg.mask(:,:)),cfg.smooth, cfg.mask_int);
    else
      maskData(:,:) = interp2(squeeze(cfg.mask(:,:)),cfg.smooth, 'nearest');
    end
  end
else
  plotData(:,:) = squeeze(data(:,:));
  if isfield(cfg, 'mask')
    maskData(:,:) = squeeze(cfg.mask(:,:));
  end
end

%%

labels = {'Training_time_(ms)' 'Testing_time_(ms)'};


try % here is the problem, I' reshaping *not* cutting the data till xaxis
  plotDatasq = reshape(squeeze(plotData(:,:)),ntest,ntest);
catch
  plotDatasq = squeeze(plotData(:,:));
end

% show the results
%surf(1:size(data,1),1:size(data,2),data');

imagesc(plotDatasq, [cfg.xaxis(1) cfg.xaxis(end)]);
set(gca,'Ydir','normal');

if isfield(cfg, 'mask')
  % set alpha according to mask field
  alpha(squeeze(maskData(:,:)));
end

fact_smooth = size(plotDatasq,1)/ntest;

xon  = 0;
%yoff = ytick_corr(end)+(fact_smooth/2);
yoff = round(numel(cfg.xaxis)*fact_smooth)+(fact_smooth/2);

ytick = cfg.xytick;
ytick_corr = round(ytick*fact_smooth)-(fact_smooth/2);

Tlab  = cfg.xaxis(ytick);
YTlab = cellfun(@num2str,num2cell(Tlab),'UniformOutput', false);
set(gca,'Ytick',ytick_corr,'YTickLabel',YTlab);


xtick = cfg.xytick;
xtick_corr = round(xtick*fact_smooth)-(fact_smooth/2);

Tlab  = cfg.xaxis(ytick);
XTlab = cellfun(@num2str,num2cell(Tlab),'UniformOutput', false);
set(gca,'Xtick',xtick_corr,'XTickLabel',XTlab);


xlabel(strrep(labels{2},'_',' '),'FontSize',17,'FontWeight','bold');
ylabel(strrep(labels{1},'_',' '),'FontSize',17,'FontWeight','bold');

hold on;
plot([yoff xon], [yoff xon], 'k:','Linewidth',2);  %change r: as needed

hold off;
colorbar();
caxis(cfg.LimAx);
set(gca,'FontSize',13,'FontWeight','bold','LineWidth',3);



end